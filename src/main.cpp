
#include <user_interface.h>

#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

#include "Ticker.h"

#define SENSOR_PIN A0

#define DEBUG false

#define DELTA_LIGHT_OPEN 40 // feel free to play with this value. When measured value is DELTA_LIGHT + averageReading, then the shutter is open and we start measure for how long.
#define DELTA_LIGHT_CLOSE 10 // when we detect light smaller than average + light close, then the shutter is closed.


U8G2_SSD1306_128X32_UNIVISION_F_SW_I2C u8g2(U8G2_R0, 5, 4, U8X8_PIN_NONE);


int charW = 6;
int charH = 12;

int w = 128;
int h = 32;

long num = 0;

int sensorValue = 0;

long totalMeasuredValue = 0;
bool measuring = true;

bool lightStarted = false;

int averageReading = 0;

double shutter = 0;
double shutterSec = 0;

void drawReady() {
    u8g2.clearBuffer();
    u8g2.setCursor(0, 22);
    u8g2.setFont(u8g2_font_unifont_t_latin);
    u8g2.print("Ready for light");
    u8g2.sendBuffer();
}

void drawResult() {

    String txt = "";

    if (shutterSec > 0) {
        txt = String(shutterSec) + " sec";
    } else if (shutter > 0) {
        if (round(shutter) == 1) {
            txt = "1 sec";
        } else {
            txt = "1/" + String(shutter) + " sec";
        }
    }

    u8g2.clearBuffer();
    u8g2.setCursor(0, 22);
    u8g2.setFont(u8g2_font_unifont_t_latin);
    u8g2.print(txt);
    u8g2.sendBuffer();
}

void callback() {
    num++;

    // measure for 2 seconds
    if (measuring && num > 16000) {


        measuring = false;

        averageReading = totalMeasuredValue/num;

        drawReady();

        if (DEBUG) {
            Serial.print("Average reading: ");
            Serial.println(averageReading);

            Serial.println();
            Serial.println("LISTENING FOR LIGHT CHANGES!");
        }
    }

    if (num > 1000000) {
        num = 0;
    }

    // read 8000 times per second
    sensorValue = analogRead(SENSOR_PIN);
    //Serial.println(sensorValue);

    // if we're measuring the light, add to the total value
    if (measuring) {
        totalMeasuredValue += sensorValue;
    } else {
        // else we check for big light differences and then mark the shutter open start
        if (!lightStarted && sensorValue > averageReading + DELTA_LIGHT_OPEN) {
            lightStarted = true;
            num = 0;

            shutter = 0;
            shutterSec = 0;

            //draw();

            if (DEBUG) {
                Serial.print("START:   sensorValue: ");
                Serial.print(sensorValue);
                Serial.println();
            }

        } else if (lightStarted && sensorValue < averageReading + DELTA_LIGHT_CLOSE) {

            lightStarted = false;

            if (num > 8000) {
                shutter = 0;
                shutterSec = ((double) num)/8000;
            } else {
                shutter = 8000 / (double) num;
                shutterSec = 0;
            }

            drawResult();

            if (DEBUG) {
                Serial.print("NUM: ");
                Serial.print(num);
                Serial.print(", ");
                Serial.print("Shutter: ");
                Serial.print(shutter);
                Serial.print(", ");
                Serial.print("ShutterSec: ");
                Serial.println(shutterSec);

                Serial.print("END  :   sensorValue: ");
                Serial.print(sensorValue);
                Serial.println();
            }
        }
    }
}

// 1000 000 is a second
// 1000 is 1/1000
// 500 is 1/2000
// 250 is 1/4000
// 125 is 1/8000
Ticker timer(callback, 125, 0, MICROS_MICROS);

void setup() {

    if (DEBUG) {
        Serial.begin(9600);
        Serial.println("Start");
    }

    system_update_cpu_freq(160);

    u8g2.begin();
    u8g2.enableUTF8Print();
    u8g2.setFontDirection(0);

    timer.start();
}


void loop() {
    timer.update();
}

